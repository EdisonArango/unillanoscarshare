package dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entidades.Persona;

public class PersonaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public Persona consultarPersona (String usuario){
		return manager.find(Persona.class, usuario);
	}
	
}
