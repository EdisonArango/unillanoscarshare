package dao;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entidades.Persona;
import entidades.Recorrido;

public class RecorridoDAO {
	
	@PersistenceContext
	EntityManager manager;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void crearRecorrido(Recorrido recorrido, String usuario){
		manager.persist(recorrido);
        Persona persona = manager.find(Persona.class, usuario);
        persona.addRecorrido(recorrido);
	}

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarLibro(Recorrido recorrido){
		manager.remove(recorrido);
	}

    public Recorrido consultarRecorrido(String id_recorrido){
		return manager.find(Recorrido.class, id_recorrido);
	}

    public void actualizarRecorrido(Recorrido recorrido, String usuario){
    	Recorrido recorridoEnBD = manager.find(Recorrido.class, recorrido.getIdRecorrido());
    	Persona persona = manager.find(Persona.class, usuario);
    	recorridoEnBD.setIdRecorrido(recorrido.getIdRecorrido());
    	recorridoEnBD.setPersona(persona);
    	recorridoEnBD.setFuente(recorrido.getFuente());
    	recorridoEnBD.setDestino(recorrido.getDestino());
    	recorridoEnBD.setFhIda(recorrido.getFhIda());
    	recorridoEnBD.setFhVuelta(recorrido.getFhVuelta());
    	recorridoEnBD.setCupos(recorrido.getCupos());
    	recorridoEnBD.setParaderos(recorrido.getParaderos());
    	recorridoEnBD.setPreferencias(recorrido.getPreferencias());
    	recorridoEnBD.setReservas(recorrido.getReservas());
	}
}
