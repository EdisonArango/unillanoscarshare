package dao;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entidades.Persona;
import entidades.Recorrido;
import entidades.Reserva;

public class ReservaDAO {
	
	@PersistenceContext
	EntityManager manager;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void crearReserva(Reserva reserva, String usuario, int idRecorrido){
		manager.persist(reserva);
        Persona persona = manager.find(Persona.class, usuario);
        persona.addReserva(reserva);
        Recorrido recorrido = manager.find(Recorrido.class, idRecorrido);
        recorrido.addReserva(reserva);
	}

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarReserva(Reserva reserva){
		manager.remove(reserva);
	}

    public Reserva consultarReserva(String id_reserva){
		return manager.find(Reserva.class, id_reserva);
	}

    public void actualizarReserva(Reserva reserva, String usuario, int idRecorrido){
    	Reserva reservaEnBD = manager.find(Reserva.class, reserva.getIdReserva());
    	Persona persona = manager.find(Persona.class, usuario);
    	Recorrido recorrido = manager.find(Recorrido.class, idRecorrido);
    	reservaEnBD.setPersona(persona);
    	reservaEnBD.setRecorrido(recorrido);
    	reservaEnBD.setParadero(reserva.getParadero());
    	reservaEnBD.setTipo(reserva.getTipo());
	}
}