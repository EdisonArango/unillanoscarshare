package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "Persona" database table.
 * 
 */
@Entity
@Table(name="\"Persona\"")
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String usuario;

	private String cargo;

	private String contrasena;

	private String nombre;

	//bi-directional many-to-one association to Recorrido
	@OneToMany(mappedBy="persona")
	private List<Recorrido> recorridos;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="persona")
	private List<Reserva> reservas;

	public Persona() {
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getContrasena() {
		return this.contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Recorrido> getRecorridos() {
		return this.recorridos;
	}

	public void setRecorridos(List<Recorrido> recorridos) {
		this.recorridos = recorridos;
	}

	public Recorrido addRecorrido(Recorrido recorrido) {
		getRecorridos().add(recorrido);
		recorrido.setPersona(this);

		return recorrido;
	}

	public Recorrido removeRecorrido(Recorrido recorrido) {
		getRecorridos().remove(recorrido);
		recorrido.setPersona(null);

		return recorrido;
	}

	public List<Reserva> getReservas() {
		return this.reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public Reserva addReserva(Reserva reserva) {
		getReservas().add(reserva);
		reserva.setPersona(this);

		return reserva;
	}

	public Reserva removeReserva(Reserva reserva) {
		getReservas().remove(reserva);
		reserva.setPersona(null);

		return reserva;
	}

}