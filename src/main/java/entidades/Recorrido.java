package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "Recorrido" database table.
 * 
 */
@Entity
@Table(name="\"Recorrido\"")
@NamedQuery(name="Recorrido.findAll", query="SELECT r FROM Recorrido r")
public class Recorrido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_recorrido")
	private Integer idRecorrido;

	private Integer cupos;

	private String destino;

	@Column(name="fh_ida")
	private String fhIda;

	@Column(name="fh_vuelta")
	private String fhVuelta;

	private String fuente;

	private String paraderos;

	private String preferencias;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="persona_conductor")
	private Persona persona;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="recorrido")
	private List<Reserva> reservas;

	public Recorrido() {
	}

	public Integer getIdRecorrido() {
		return this.idRecorrido;
	}

	public void setIdRecorrido(Integer idRecorrido) {
		this.idRecorrido = idRecorrido;
	}

	public Integer getCupos() {
		return this.cupos;
	}

	public void setCupos(Integer cupos) {
		this.cupos = cupos;
	}

	public String getDestino() {
		return this.destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getFhIda() {
		return this.fhIda;
	}

	public void setFhIda(String fhIda) {
		this.fhIda = fhIda;
	}

	public String getFhVuelta() {
		return this.fhVuelta;
	}

	public void setFhVuelta(String fhVuelta) {
		this.fhVuelta = fhVuelta;
	}

	public String getFuente() {
		return this.fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	public String getParaderos() {
		return this.paraderos;
	}

	public void setParaderos(String paraderos) {
		this.paraderos = paraderos;
	}

	public String getPreferencias() {
		return this.preferencias;
	}

	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Reserva> getReservas() {
		return this.reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public Reserva addReserva(Reserva reserva) {
		getReservas().add(reserva);
		reserva.setRecorrido(this);

		return reserva;
	}

	public Reserva removeReserva(Reserva reserva) {
		getReservas().remove(reserva);
		reserva.setRecorrido(null);

		return reserva;
	}

}