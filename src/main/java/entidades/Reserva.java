package entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Reserva" database table.
 * 
 */
@Entity
@Table(name="\"Reserva\"")
@NamedQuery(name="Reserva.findAll", query="SELECT r FROM Reserva r")
public class Reserva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_reserva")
	private Integer idReserva;

	private String paradero;

	private String tipo;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="persona_pasajero")
	private Persona persona;

	//bi-directional many-to-one association to Recorrido
	@ManyToOne
	@JoinColumn(name="id_recorrido")
	private Recorrido recorrido;

	public Reserva() {
	}

	public Integer getIdReserva() {
		return this.idReserva;
	}

	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}

	public String getParadero() {
		return this.paradero;
	}

	public void setParadero(String paradero) {
		this.paradero = paradero;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Recorrido getRecorrido() {
		return this.recorrido;
	}

	public void setRecorrido(Recorrido recorrido) {
		this.recorrido = recorrido;
	}

}