package modelo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MRecorrido {

	private MPersona anfitrion;
	private String fecha;
	private String fechaCompleta;
	private String horaIda;
	private String horaVuelta;
	private String asientosDisponibles;
	private String lugarSalida;
	private String lugarDestino;
	private ArrayList<String> paradas;
	
	public MRecorrido(MPersona anfitrion, String fecha, String horaIda,
			String horaVuelta, String asientosDisponibles, String lugarSalida,
			String lugarDestino, ArrayList<String> paradas) {
		this.anfitrion = anfitrion;
		this.fecha = fecha;
		this.horaIda = horaIda;
		this.horaVuelta = horaVuelta;
		this.asientosDisponibles = asientosDisponibles;
		this.lugarSalida = lugarSalida;
		this.lugarDestino = lugarDestino;
		this.paradas = paradas;
		
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(this.fecha);
			DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy");
			this.fechaCompleta = dateFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public MPersona getAnfitrion() {
		return anfitrion;
	}

	public void setAnfitrion(MPersona anfitrion) {
		this.anfitrion = anfitrion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHoraIda() {
		return horaIda;
	}

	public void setHoraIda(String horaIda) {
		this.horaIda = horaIda;
	}

	public String getHoraVuelta() {
		return horaVuelta;
	}

	public void setHoraVuelta(String horaVuelta) {
		this.horaVuelta = horaVuelta;
	}

	public String getAsientosDisponibles() {
		return asientosDisponibles;
	}

	public void setAsientosDisponibles(String asientosDisponibles) {
		this.asientosDisponibles = asientosDisponibles;
	}

	public String getLugarSalida() {
		return lugarSalida;
	}

	public void setLugarSalida(String lugarSalida) {
		this.lugarSalida = lugarSalida;
	}

	public String getLugarDestino() {
		return lugarDestino;
	}

	public void setLugarDestino(String lugarDestino) {
		this.lugarDestino = lugarDestino;
	}

	public ArrayList<String> getParadas() {
		return paradas;
	}

	public void setParadas(ArrayList<String> paradas) {
		this.paradas = paradas;
	}

	public String getFechaCompleta() {
		return fechaCompleta;
	}

	public void setFechaCompleta(String fechaCompleta) {
		this.fechaCompleta = fechaCompleta;
	}
	
}
