package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import dao.RecorridoDAO;
import entidades.Recorrido;

@Stateless
public class PublicacionBean implements iPublicacionBean{
	@Inject
	RecorridoDAO recorridoDAO;
	
	public PublicacionBean(){}
	
	public void agregarRecorrido(int idRecorrido, String persona, String fuente, String destino, String fhIda, String fhVuelta,
			int cupos, String paraderos, String preferencias) {
		Recorrido objRecorrido = new Recorrido();
		objRecorrido.setIdRecorrido(idRecorrido);
		//objRecorrido.setPersona(persona);
		objRecorrido.setFuente(fuente);
		objRecorrido.setDestino(destino);
		objRecorrido.setFhIda(fhIda);
		objRecorrido.setFhVuelta(fhVuelta);
		objRecorrido.setCupos(cupos);
		objRecorrido.setParaderos(paraderos);
		objRecorrido.setPreferencias(preferencias);
		
		recorridoDAO.crearRecorrido(objRecorrido, persona);
	}
}
