package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import dao.ReservaDAO;
import entidades.Reserva;

@Stateless
public class ReservaBean implements iReservaBean{
	@Inject
	ReservaDAO ReservaDAO;
	
	public ReservaBean(){}
	
	public void agregarReserva(int idReserva, int idRecorrido, String persona, String paradero, String tipo) {
		Reserva objReserva = new Reserva();
		objReserva.setIdReserva(idReserva);
		//objReserva.setRecorrido(idRecorrido);
		//objReserva.setPersona(persona);
		objReserva.setParadero(paradero);
		objReserva.setTipo(tipo);
		
		ReservaDAO.crearReserva(objReserva, persona, idRecorrido);
	}
}
