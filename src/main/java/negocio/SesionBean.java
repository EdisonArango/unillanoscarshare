package negocio;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import dao.PersonaDAO;
import entidades.Persona;

@Stateless
public class SesionBean implements iSesionBean{

	@Inject
	PersonaDAO personaDAO;
	
	@Override
	public String login(String usuario, String contrasenia) {
		Persona encontrado = personaDAO.consultarPersona(usuario);
		
		FacesContext context = FacesContext.getCurrentInstance();
		if(encontrado != null){
			if (usuario.equals(encontrado.getUsuario()) && contrasenia.equals(encontrado.getContrasena())){
				context.getExternalContext().getSessionMap().put("usuario", encontrado);
				return "index";
			}
			else{
				return "index";
			}
		}
		else{
			return "index";
		}
	}
	
	@Override
	public String cerrarSesion(){
		FacesContext context = FacesContext.getCurrentInstance();
		Persona actual = (Persona)(context.getExternalContext().getSessionMap().get("usuario"));
		System.out.print(actual.getUsuario());
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "index";
	}

	@Override
	public String getUsuarioSesion(){
		FacesContext context = FacesContext.getCurrentInstance();
		Persona actual = (Persona)(context.getExternalContext().getSessionMap().get("usuario"));
		return actual.getUsuario();
	}

}
