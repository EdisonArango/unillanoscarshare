package negocio;

import javax.ejb.Remote;

@Remote
public interface iPublicacionBean {
	
	public void agregarRecorrido(int idRecorrido, String persona, String fuente, String destino, String fhIda, String fhVuelta,
			int cupos, String paraderos, String preferencias);
}

