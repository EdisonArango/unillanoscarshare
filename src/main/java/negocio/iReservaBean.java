package negocio;

import javax.ejb.Remote;

@Remote
public interface iReservaBean {
	
	public void agregarReserva(int idReserva, int idRecorrido, String persona, String paradero, String tipo);
}