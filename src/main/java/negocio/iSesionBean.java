package negocio;

public interface iSesionBean {

	public String login(String usuario, String contrasenia);
	public String cerrarSesion();
	public String getUsuarioSesion();
}
