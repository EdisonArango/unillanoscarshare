package presentacion;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import modelo.MPersona;
import modelo.MRecorrido;

@ManagedBean
public class BusquedaController {
	
	@ManagedProperty(value="#{busquedaModel}")
	private BusquedaModel busqueda;
	private String respuesta;
	private ArrayList<MRecorrido> recorridos;
	private ArrayList<MRecorrido> encontrados;
	
	
	public BusquedaController() {
		this.recorridos = new ArrayList<MRecorrido>();
		ArrayList<String> paradas = new ArrayList<String>();
		paradas.add("Alkosto");
		paradas.add("U. Cooperativa");
		recorridos.add(new MRecorrido(new MPersona("Edison Arango","Profesor"),
					"05/11/2015","08:15 AM", "05:20 PM", "2",
					"Villavicencio", "Unillanos",
					paradas));
		
		paradas = new ArrayList<String>();
		paradas.add("Las Américas");
		paradas.add("Éxito");
		paradas.add("Unicentro");
		recorridos.add(new MRecorrido(new MPersona("Carlos Perez","Estudiante"),
					"05/11/2015","08:15 AM", "05:20 PM", "1",
					"Villavicencio", "Unillanos",
					paradas));
		
		paradas = new ArrayList<String>();
		paradas.add("U. Cooperativa");
		paradas.add("Makro");
		recorridos.add(new MRecorrido(new MPersona("Juan Castro","Administrativo"),
					"06/11/2015","07:20 AM", "04:20 PM", "1",
					"Villavicencio", "Unillanos",
					paradas));
		
		paradas = new ArrayList<String>();
		paradas.add("Postobon");
		recorridos.add(new MRecorrido(new MPersona("Jonathan Acosta","Estudiante"),
					"05/11/2015","07:20 AM", "04:20 PM", "3",
					"Unillanos", "Villavicencio",
					paradas));
	}
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public BusquedaModel getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(BusquedaModel busqueda) {
		this.busqueda = busqueda;
	}
	
	public ArrayList<MRecorrido> getRecorridos() {
		return recorridos;
	}

	public void setRecorridos(ArrayList<MRecorrido> recorridos) {
		this.recorridos = recorridos;
	}
	
	public ArrayList<MRecorrido> getEncontrados() {
		return encontrados;
	}

	public void setEncontrados(ArrayList<MRecorrido> encontrados) {
		this.encontrados = encontrados;
	}

	public String buscar (){
		String desde = busqueda.getDesde();
		String hasta = busqueda.getHasta();
		String fecha = busqueda.getFecha();
		
		if(!desde.equalsIgnoreCase("villavicencio") && !desde.equalsIgnoreCase("unillanos")){
			this.respuesta = "El origen ingresado es incorrecto";
			return "busquedaError";
		}
		else if(!hasta.equalsIgnoreCase("villavicencio") && !hasta.equalsIgnoreCase("unillanos")){
			this.respuesta = "El destino ingresado es incorrecto";
			return "busquedaError";
		}
		else if(!fechaValida(fecha)){
			this.respuesta = "La fecha ingresada es incorrecta";
			return "busquedaError";
		}
		
		ArrayList<MRecorrido> encontrados = new ArrayList<MRecorrido>();
		for (int i = 0; i < recorridos.size(); i++) {
			MRecorrido actual = recorridos.get(i);
			if(actual.getFecha().equals(fecha) && actual.getLugarSalida().equalsIgnoreCase(desde) 
					&& actual.getLugarDestino().equalsIgnoreCase(hasta)){
				encontrados.add(actual);
			}
		}
		this.encontrados = encontrados;
		
		return "busqueda";
	}

	public boolean fechaValida(String fecha){
		String[] partesFecha = fecha.split("/");
		if(partesFecha==null || partesFecha.length!=3){
			return false;
		}
		
		String dia = partesFecha[0];
		String mes = partesFecha[1];
		String year = partesFecha[2];
		
		if(dia.length()!=2 || mes.length()!=2 || year.length()!=4){
			return false;
		}
		
		if(!isNumeric(dia) || !isNumeric(mes) || !isNumeric(year)){
			return false;
		}
		
		return true;
	}
	
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
}
