package presentacion;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import negocio.iSesionBean;

@ManagedBean
public class LoginController {
	
	@ManagedProperty(value="#{loginModel}")
	private LoginModel loginModel;
	
	@EJB 
	iSesionBean sesionBean;

	public LoginModel getLoginModel() {
		return loginModel;
	}

	public void setLoginModel(LoginModel loginModel) {
		this.loginModel = loginModel;
	}
	
	public String login(){
		String usuario = loginModel.getUsuario();
		String contrasenia = loginModel.getContrasenia();
		return sesionBean.login(usuario, contrasenia);
	}
	
	public String cerrarSesion(){
		return sesionBean.cerrarSesion();
	}
	
}
