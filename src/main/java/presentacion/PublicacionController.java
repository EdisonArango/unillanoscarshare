package presentacion;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import negocio.iPublicacionBean;

@ManagedBean
public class PublicacionController {
	
	@ManagedProperty(value="#{PublicacionModel}")
	private PublicacionModel publicar;
	
	@EJB
	iPublicacionBean publicacionBean;
	
	public PublicacionController() {
	}
	public PublicacionModel getPublicacion() {
		return publicar;
	}
	public void setPublicacion(PublicacionModel publicar) {
		this.publicar = publicar;
	}
	public String guardar(){
		int idRecorrido = publicar.getIdRecorrido();
		String persona = publicar.getPersona(); 
		String fuente = publicar.getFuente();
		String destino = publicar.getDestino();
		String fhIda = publicar.getFhIda();
		String fhVuelta = publicar.getFhVuelta();
		int cupos = publicar.getCupos();
		String paraderos = publicar.getParaderos();
		String preferencias = publicar.getPreferencias();
		String rta = "publicar";
		try{
			publicacionBean.agregarRecorrido(idRecorrido, persona, fuente, destino, fhIda, fhVuelta, cupos, paraderos, preferencias);
			rta = "publicar";
		}catch(Exception ex){
			rta = "publicarError";
		}
		return rta;
   }
}
