package presentacion;


public class PublicarModel {
	
	private int cupos;
	private String fuente;
	private String destino;
	private String fhIda;
	private String fhVuelta;
	private String paraderos;
	private String preferencias;
	
	public int getCupos() {
		return cupos;
	}
	public void setCupos(int cupos) {
		this.cupos = cupos;
	}
	public String getFuente() {
		return fuente;
	}
	public void setFuente(String fuente) {
		this.fuente = fuente;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getFhIda() {
		return fhIda;
	}
	public void setFhIda(String fhIda) {
		this.fhIda = fhIda;
	}
	public String getFhVuelta() {
		return fhVuelta;
	}
	public void setFhVuelta(String fhVuelta) {
		this.fhVuelta = fhVuelta;
	}
	public String getParaderos() {
		return paraderos;
	}
	public void setParaderos(String paraderos) {
		this.paraderos = paraderos;
	}
	public String getPreferencias() {
		return preferencias;
	}
	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}
	
	
}
