package presentacion;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import negocio.iReservaBean;

@ManagedBean
public class ReservaController {
	
	@ManagedProperty(value="#{ReservaModel}")
	private ReservaModel reservar;
	
	@EJB
	iReservaBean ReservaBean;
	
	public ReservaController() {
	}
	public ReservaModel getReserva() {
		return reservar;
	}
	public void setReserva(ReservaModel reservar) {
		this.reservar = reservar;
	}
	public String guardar(){
		int idReserva = reservar.getIdReserva();
		int idRecorrido = reservar.getIdRecorrido(); 
		String persona = reservar.getPersona();
		String paradero = reservar.getParadero();
		String tipo = reservar.getTipo();
		String rta = "casoReservaNoAgregada";
		try{
			ReservaBean.agregarReserva(idReserva, idRecorrido, persona, paradero, tipo);
			rta = "reserva";
		}catch(Exception ex){
			rta = "reservaError";
		}
		return rta;
   }
}
