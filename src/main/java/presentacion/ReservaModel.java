package presentacion;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ReservaModel {
	private int idReserva;
	private int idRecorrido; 
	private String persona;
	private String paradero;
	private String tipo;
	public int getIdReserva() {
		return idReserva;
	}
	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}
	public int getIdRecorrido() {
		return idRecorrido;
	}
	public void setIdRecorrido(int idRecorrido) {
		this.idRecorrido = idRecorrido;
	}
	public String getPersona() {
		return persona;
	}
	public void setPersona(String persona) {
		this.persona = persona;
	}
	public String getParadero() {
		return paradero;
	}
	public void setParadero(String paradero) {
		this.paradero = paradero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
